<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\PublicController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'home'])->name('home');
Route::get('/dashboard', [ArticleController::class, 'index'])->name('dashboard');
Route::get('/article/create', [ArticleController::class, 'create'])->name('newArticle');
Route::post('/article/store', [ArticleController::class, 'store'])->name('storeArticle');
Route::get('/article/detail/{article}', [ArticleController::class, 'show'])->name('article.detail');
Route::get('/article/edit/{article}', [ArticleController::class, 'edit'])->name('editArticle');
Route::put('/article/update/{article}', [ArticleController::class, 'update'])->name('updateArticle');
Route::delete('/article/delete/{article}', [ArticleController::class, 'destroy'])->name('deleteArticle');
// Route::get('/profile/{user}', [PublicController::class, 'showProfile'])->name('userProfile');
Route::get('/searchText', [ArticleController::class, 'searchText'])->name('searchText');
