<?php

namespace App\Models;

use App\Models\User;
use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Article extends Model
{
    use HasFactory;
    use Searchable;

    protected $fillable = ['title', 'description', 'date', 'city', 'state', 'img', 'user_id'];

    public function toSearchableArray()
    {
        $array = [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'city' => $this->city,
            'state' => $this->state,
        ];

        return $array;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    static public function getCropPic($article)
    {
        $filepath = $article->img;
        $cropPath = '\storage\media\article\crop_' . basename($filepath);

        return $cropPath;
    }
}
