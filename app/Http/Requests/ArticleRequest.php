<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'description' => 'required',
            'date' => 'required',
            'city' => 'required',
            'state' => 'required',
            'img' => 'required|image'
        ];
    }

    public function messages()
    {
        return [
            'img.required' => 'The image is required',
        ];
    }
}
