<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ArticleRequest;
use App\Jobs\ResizeImage;
use Spatie\Activitylog\Models\Activity;

class ArticleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $countries = Country::all();
        $articles = Auth::user()->articles()
            ->orderBy('created_at', 'desc')
            ->get();

        return view('dashboard', compact('articles', 'countries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all();
        return view('article.create', compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {
        $user = Auth::user();
        $article = $user->articles()->create([
            'title' => $request->title,
            'description' => $request->description,
            'date' => $request->date,
            'city' => $request->city,
            'state' => $request->state,
            'img' => $request->file('img')->store('public/media/article')

        ]);

        $filePath = $article->img;
        dispatch(new ResizeImage($filePath, 350, 400));


        return redirect(route('dashboard'))->with('message', Auth::user()->name . ', il tuo post è stato salvato');
    }



    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        $article = Auth::user()->articles()->find($article->id);


        return view('article.show', compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        $countries = Country::all();
        $article = Auth::user()->articles()->find($article->id);
        return view('article.edit', compact('article', 'countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        $article = Auth::user()->articles()->find($article->id);
        $article->title = $request->title;
        $article->description = $request->description;
        $article->date = $request->date;
        $article->city = $request->city;
        $article->state = $request->state;
        if ($request->file('img')) {
            $article->img = $request->file('img')->store('public/media/article');
        }
        $article->save();

        activity()->performedOn($article)->causedBy(Auth::user())->log('update');
        //$lastActivity = Activity::all()->last();
        return redirect(route('dashboard'))->with('message', 'Il tuo post è stato modificato');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {

        $article = Auth::user()->articles()->find($article->id);
        $article->delete();
        return redirect(route('dashboard'))->with('message', 'Il tuo post è stato eliminato');
    }

    public function searchText(Request $request)
    {
        $q = $request->input('q');
        $userId = Auth::user()->id;
        $state = $request->state;
        // $articles = Article::search($q)->where('user_id', $userId)->get();
        $articles = Auth::user()->articles()
            ->orderBy('created_at', 'desc');

        if ($q == null) {
            if ($state != 'Seleziona uno Stato') {
                $articles = $articles
                    ->where('state', $state)
                    ->get();
            } else {
                $articles = $articles->get();
            }
        } else {
            if ($state != 'Seleziona uno Stato') {
                $articles = Article::search($q)->where('user_id', $userId)
                    ->where('state', $state)
                    ->get();
            } else {
                $articles = Article::search($q)->where('user_id', $userId)->get();
            }
        }

        return view('article.searchTextResults', compact('q', 'articles'));
    }
}
