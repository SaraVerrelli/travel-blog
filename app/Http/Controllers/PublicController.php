<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PublicController extends Controller
{
    public function home()
    {
        if (Auth::user()) {
            $user = Auth::user();
            $articles = $user->articles()
                ->orderBy('created_at', 'desc')
                ->take(4)
                ->get();
            return view('home', compact('articles'));
        } else {
            return view('home');
        }
    }

    public function showProfile(User $user)
    {
        $user = Auth::user();
        return view('profile', compact('user'));
    }
}
