<x-layout>
    <header>
        <h1 class="text-center">Il tuo profilo</h1>
    </header>
    <main class="container-fluid">
        <div class="row mx-auto">
            <div class="col-12 col-md-6 col-lg-3 bg-danger p-5 offset-lg-1">
                <img src="/media/avatar.png" alt="your profile picture" class="img-fluid">
               <div class="row d-flex my-5 text-center">
                <h5>{{Auth::user()->name}}</h5>
                <h6 class="mt-3">{{Auth::user()->email}}</h6>
               </div>
            </div>
            <div class="col-12 col-md-6 col-lg-5 offset-lg-1 mt-3">
                <div class="card shadow p-2">
                    <div class="row">
                        <div class="col-6">
                            <h5>Nome:</h5>
                        </div>
                        <div class="col-6">
                            <h5>{{Auth::user()->name}}</h5>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </main>
    
</x-layout>