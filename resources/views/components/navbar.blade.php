<nav class="navbar navbar-expand-md bg-acc navbar-dark">
  <div class="container-fluid ml-auto">
    <i class="fas fa-2x fa-globe-europe cl-white">
      <a class="navbar-brand" href="{{route('home')}}"></i>Travel Blog</a>
      @auth
      <ul class="navbar-nav mb-2 mb-lg-0 ms-auto">
        <li class="nav-item">
          <a class="nav-link mx-3 cl-white">Ciao, {{Auth::user()->name}}</a>
        </li>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsCollapseItems" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarsCollapseItems">
          <li class="nav-item">
          <a href="{{route('dashboard')}}" class="nav-link mx-3">Dashboard</a>
        </li>
        <li class="nav-item">
          <a href="{{route('newArticle')}}" class="nav-link mx-3">Nuovo Post</a>
        </li>
        <li class="nav-item">
          <a class="nav-link mx-3" href="{{route('logout')}}" onclick="event.preventDefault();
          document.getElementById('form-logout').submit();">Logout</a>
          <form method="POST" action="{{route('logout')}}" id="form-logout">
            @csrf
          </form> 
          
        </li>
        
        </div>
        
        @endauth
        @guest
        <ul class="navbar-nav mb-2 mb-lg-0 ms-auto">
          
          <li class="nav-item">
            <a href="{{route('login')}}" class="nav-link active">Login</a>
          </li>
        </ul>
        @endguest          
        
      </ul>
  </div>
</nav>
      