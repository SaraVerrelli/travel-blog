<x-layout>
    
    <header class="container-flex">
        <div class="bg-beige p-4 mb-0 header-img">
            <div class="position-header-text">
                <h1 class="header-text">The space to keep your memories. </h1> 
                <p class="header-p">Make every day count</p>
                <a href="{{route('newArticle')}}"><button class="btn btn-post mt-2">+ New post</button></a>
                
            </div>
            
            
        </div>     
    </header>
    
    <main class="container-flex p-4 bg-grey-cs">
        <div class="row py-3">
            <h3 class="fw-bolder fs-2 pb-5">YOUR most recent MEMORIES.</h3>
            
            @if (Auth::user())
            @foreach ($articles as $article)
            <div class="col-12 col-xs-6 col-sm-4 col-md-3 mt-4 d-flex justify-content-center">
                <div class="card-cs shadow bg-white">
                    
                    <img src="{{$article->getCropPic($article)}}" class="img-fluid rounded" alt="picture taken in {{$article->state}}">
                    <div class="card-body">
                        <h5 class="card-title fw-bolder">{{$article->title}}</h5>
                        <p class="card-text">{{$article->state}}</p>
                        <a href="{{route('article.detail', compact('article'))}}" class="btn btn-post2">Go here</a>
                    </div>
                </div>
            </div>
            @endforeach 
            @else
            <div class="col-12">
                <p>To see your posts log-in. <a href="{{route('login')}}"><button href="{{route('login')}}" class="btn btn-post2 ms-3">Login</button></a>  
                </p>
            </div>
            
            
            @endif
        </div>
    </main>
</x-layout>