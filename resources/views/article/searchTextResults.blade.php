<x-layout>
    <div class="container-fluid wide">
        <div class="row mx-auto">
            <h1>Results for: {{$q}}</h1>
        </div>
        <div class="row mx-auto">
            @if (!$articles->first())
            <p>No posts for this search</p>
                
            @endif
            @foreach ($articles as $article)
            <div class="col-12 col-md-11">
                <div class="row border g-0 rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                    <div class="col-auto d-none d-lg-block">
                        <img src="{{$article->getCropPic($article)}}" class="img-fluid rounded img-sm" alt="picture taken in {{$article->state}}">                                
                    </div>
                    <div class="col p-4 d-flex flex-column position-static">
                        <h3 class="mb-0">{{$article->title}}</h3>
                        <hr>
                        <strong class="d-inline-block mb-2">{{$article['city']}}</strong>
                        <div class="mb-1 text-muted">{{$article['date']}}</div>
                        <a href="{{route('article.detail', compact('article'))}}" class="stretched-link align-self-end">Vai al post</a>
                    </div>
                    
                </div>
            </div>
                
            @endforeach
        </div>
    </div>
</x-layout>