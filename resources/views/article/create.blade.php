<x-layout>
    <x-slot name="title">Nuovo Articolo</x-slot>
    
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 offset-md-3 mt-5">
                <h1>Salva un nuovo posto</h1>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif  
                <form method="POST" action="{{route('storeArticle')}}" enctype="multipart/form-data"> 
                    @csrf
                    <div class="my-3">
                        <label for="InputTitle" class="form-label">Titolo</label>
                        <input type="text" class="form-control" id="InputTitle" name="title" value="{{old("title")}}">
                    </div>
                    <div class="mb-3">
                        <label for="InputDescription" class="form-label">Descrivi la tua esperienza</label>
                        <textarea class="form-control" name="description" id="InputDescription">{{old("description")}}</textarea>
                    
                    </div>
                    <div class="mb-3">
                        <label for="InputDate" class="form-label">Quando è iniziato il viaggio?</label>
                        <input class="form-control" type="date" name="date" id="InputDate" value="{{old("title")}}">
                    </div>
                   
                    <div class="mb-3">
                        <label for="InputState" class="form-label">Stato</label>
                            <select class="form-select" aria-label="Default select example" name="state" value="{{old("state")}}">
                                <option selected></option>
                                @foreach ($countries as $country)
                                <option value="{{$country->name}}">{{$country->name}}</option>  
                                @endforeach
                            </select>
                    </div>
                    <div class="mb-3">
                        <label for="InputCity" class="form-label">Città</label>
                        <input type="text" id="InputCity" class="form-control" name="city" value="{{old("city")}}">
                    </div>
                    <div class="mb-3">
                        <label for="InputImmagine" class="form-label">Carica un'immagine</label>
                        <input type="file" class="form-control" id="InputImmagine" name="img">
                    </div>
                    <button type="submit" class="btn btn-primary">Salva</button>
                </form>
                
            </div>
        </div>
    </div>
    
    
    
    
    
    
</x-layout>
