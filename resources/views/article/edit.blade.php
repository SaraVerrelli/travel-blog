<x-layout>
    <div class="container-fluid">
        <div class="row">
            <h1 class="text-center">{{$article['title']}}</h1>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif 
            <div class="col-6 offset-3">
                <form method="POST" action="{{route('updateArticle', compact('article'))}}" enctype="multipart/form-data"> 
                    @csrf
                    @method('put')
                    <div class="my-3">
                        <label for="InputTitle" class="form-label">Titolo</label>
                        <input type="text" class="form-control" id="InputTitle" name="title" value="{{$article['title']}}">
                    </div>
                    <div class="mb-3">
                        <label for="InputDescription" class="form-label">Descrivi la tua esperienza</label>
                        <textarea class="form-control" name="description" id="InputDescription">{{$article['description']}}</textarea>
                        
                    </div>
                    <div class="mb-3">
                        <label for="InputDate" class="form-label">Quando è iniziato il viaggio?</label>
                        <input type="date" class="form-control" name="date" id="InputDate" value="{{old('title')}}">
                    </div>
                    <div class="mb-3">
                        <label for="InputCity" class="form-label">Città</label>
                        <input type="text" id="InputCity" class="form-control" name="city" value="{{$article['city']}}">
                    </div> 
                    <div class="mb-3">
                        <label for="InputState" class="form-label">Stato</label>
                        <select class="form-select" id="state" name="state">

                            @foreach ($countries as $country)
                            <option value="{{$country->name}}">{{$country->name}}</option>  
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <img src="{{Storage::url($article->img)}}" alt="">
                    </div>
                    <div class="mb-3">
                        <label for="InputImmagine" class="form-label">Carica un'immagine</label>
                        <input type="file" class="form-control" id="InputImmagine" name="img">
                    </div>
                    <button type="submit" class="btn btn-primary">Salva</button>
                </form>
                
            </div>
            
        </div>
    </div>
    
    
</x-layout>