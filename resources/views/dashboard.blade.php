<x-layout>
    <x-slot name="title">Travel Blog</x-slot>
    <nav class="navbar navbar-expand-lg navbar-light bg-grey-cs">
        <div class="container-fluid">
            
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                <form action="{{route('searchText')}}" method="GET" class="d-flex">
                    
                    <li class="nav-item mx-1">
                        <select class="form-select" name="state">
                                <option selected>Seleziona uno Stato</option>
                                @foreach ($countries as $country)
                                <option value="{{$country->name}}">{{$country->name}}</option>    
                            
                            
                            @endforeach                             
                        </select>
                    </li>
                    <li class="nav-item mx-1">
                        <input class="form-control" name="q" type="text" placeholder="Cerca testo">
                        
                    </li>
                    <li class="nav-item mx-1">
                        <button class="btn btn-cerca" type="submit">Cerca</button>
                        
                    </li>
                </form>
                
                
            </ul>
            
            
        </div>
    </nav>
    
    <div class="container-fluid py-5 bg-grey-dashboard">
        <div class="row">
            <h1 class="text-center">Your Dashboard</h1>
            @if (session('message'))
            <div class="alert alert-success">
                {{session('message')}}
            </div>
            
            @endif
        </div>
    </div>
    <main class="container-fluid bg-grey-dashboard">
        <div class="row">
            <div class="col-12 col-md-9">
                <div class="row">
                    @foreach ($articles as $article)
                    <div class="col-12 col-md-11 offset-md-1">
                        <div class="row border g-0 rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                            <div class="col-auto d-none d-lg-block">
                                <img src="{{$article->getCropPic($article)}}" class="img-fluid rounded img-sm" alt="picture taken in {{$article->state}}">                                
                            </div>
                            <div class="col p-4 d-flex flex-column position-static">
                                <h3 class="mb-0">{{$article->title}}</h3>
                                <hr>
                                <strong class="d-inline-block mb-2">{{$article['city']}}</strong>
                                <div class="mb-1 text-muted">{{$article['date']}}</div>
                                <a href="{{route('article.detail', compact('article'))}}" class="stretched-link align-self-end">Vai al post</a>
                            </div>
                            
                        </div>
                    </div>
                    
                    @endforeach
                    
                </div>
                
            </div>
            <div class="col-3 sidebar">
                <h4>I tuoi post</h4>                
                <div class="overflow-auto h-custom">
                    <div class="d-flex flex-column align-items-stretch flex-shrink-0 bg-white">
                        <div class="list-group list-group-flush border-bottom scrollarea">
                            @foreach ($articles as $article)
                            <a href="{{route('article.detail', compact('article'))}}" class="list-group-item list-group-item-action py-3 lh-tight" aria-current="true">
                                <div class="d-flex w-100 align-items-center justify-content-between">
                                    <strong class="mb-1">{{$article->title}}</strong>
                                    <small>{{$article->date}}</small>
                                </div>
                                <div class="col-10 mb-1 small">{{$article->state}}</div>
                            </a>
                            
                            @endforeach
                            
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </main>
    
</x-layout>